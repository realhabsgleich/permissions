package de.habsgleich.permissions.language;

import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

class LanguageTest {

  @Test
  void testGerman() {
    Language language = Language.create(new File("src/main/resources/languages/de_DE.properties"));

    assertNotEquals(language.get("prefix"), "N/A");
    assertEquals(language.get("group-created"), "%prefix% §aDu hast die Gruppe {0} erstellt");
  }

  @Test
  void testEnglish() {
    Language language = Language.create(new File("src/main/resources/languages/en_US.properties"));

    assertNotEquals(language.get("prefix"), "N/A");
    assertEquals(language.get("group-created"), "%prefix% §aYou created the group {0}");
  }
}