package de.habsgleich.permissions.utils;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

class TimeFormatterTest {

  @Test
  void testFormatting() {
    long threeSeconds = TimeUnit.SECONDS.toMillis(3);
    long threeSecondsFormatted = TimeFormatter.of(new String[]{"3sec"});
    assertEquals(threeSeconds, threeSecondsFormatted);

    long fiveMinutes = TimeUnit.MINUTES.toMillis(5);
    long fiveMinutesFormatted = TimeFormatter.of(new String[]{"5min"});
    assertEquals(fiveMinutes, fiveMinutesFormatted);

    long oneDayAndTwentyHours = TimeUnit.DAYS.toMillis(1) + TimeUnit.HOURS.toMillis(20);
    long oneDayAndTwentyHoursFormatted = TimeFormatter.of(new String[]{"1d", "20h"});
    assertEquals(oneDayAndTwentyHours, oneDayAndTwentyHoursFormatted);
  }
}
