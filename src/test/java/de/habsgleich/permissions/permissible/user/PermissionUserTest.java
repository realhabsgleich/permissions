package de.habsgleich.permissions.permissible.user;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class PermissionUserTest {

  @Test
  void formatExpire() {
    PermissionUser user = new PermissionUser(UUID.randomUUID(), null, Optional.of(0L));

    assertEquals(user.formatExpire(), "01.01.1970 01:00");
  }
}