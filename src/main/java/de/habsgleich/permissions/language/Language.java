package de.habsgleich.permissions.language;

import lombok.Data;
import lombok.SneakyThrows;

import java.io.File;
import java.io.FileReader;
import java.text.MessageFormat;
import java.util.Properties;

/**
 * Class to hold locale properties
 *
 * <p>Created: 14.10.2022
 *
 * @author HabsGleich
 */
@Data
public final class Language {
  private final File file;
  private final Properties properties;
  private final String tag;

  /**
   * Creates an {@link Language} instance from a file
   *
   * @param file properties file with language context
   * @return {@link Language} instance
   */
  @SneakyThrows
  public static Language create(File file) {
    if (file == null) {
      throw new NullPointerException("Cloud not found language file");
    }

    // Load language content
    final Properties propertiesFile = new Properties();
    propertiesFile.load(new FileReader(file));

    return new Language(file, propertiesFile, file.getName().split("\\.")[0]);
  }

  /**
   * Returns the context behind a language key
   *
   * @param key language key
   * @param args to replace the java fields in the string
   * @return language context behind the key
   */
  public String get(String key, Object... args) {
    final String message = properties.getProperty(key);
    if (message == null) {
      return "N/A";
    }
    return MessageFormat.format(message, args);
  }
}
