package de.habsgleich.permissions.language;

import com.google.common.base.Preconditions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Manage the locales
 *
 * <p>Created: 14.10.2022
 *
 * @author HabsGleich
 */
public final class LanguageManager {
  private static final Path LANGUAGE_FOLDER = Path.of("plugins/permissions/language");

  private static final Map<String, Language> languageMap = new HashMap<>();
  private static Language usageLanguage;

  /** Copy default language files into the language folder */
  public static void copyInternalFiles(Collection<String> languages) {
    // Load default language files
    if (Files.notExists(LANGUAGE_FOLDER)) {
      try {
        Files.createDirectories(LANGUAGE_FOLDER);
        for (String language : languages) {
          InputStream file =
              LanguageManager.class.getResourceAsStream("/languages/" + language + ".properties");
          Files.copy(
              Preconditions.checkNotNull(file),
              new File(LANGUAGE_FOLDER.toFile(), language + ".properties").toPath());
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * Load language files into {@link Language} instances
   *
   * @param defaultLanguage language to use as default
   * @param languages languages that will be loaded
   */
  public static void load(String defaultLanguage, Collection<String> languages) {
    for (String lang : languages) {
      final Language language =
          Language.create(new File(LANGUAGE_FOLDER.toFile(), lang + ".properties"));
      if (defaultLanguage.equals(language.getTag())) {
        usageLanguage = language;
      }
      languageMap.put(lang, language);
      System.out.println("Loaded language " + lang);
    }
    Preconditions.checkNotNull(usageLanguage, "No language was set as usageLanguage");
  }

  /**
   * Reloads language files
   *
   * @param defaultLanguage language to use as default
   * @param languages languages that will be loaded
   */
  public static void reload(String defaultLanguage, Collection<String> languages) {
    languageMap.clear();
    usageLanguage = null;
    load(defaultLanguage, languages);
  }

  /**
   * @see Language#get(String, Object...)
   */
  public static String get(String key, Object... replacements) {
    String message = usageLanguage.get(key);
    if (message.contains("%prefix%")) {
      message = message.replace("%prefix%", get("prefix"));
    }

    final Object[] strings =
        Stream.of(replacements)
            .map(replacement -> replacement == null ? "null" : replacement.toString())
            .toArray(String[]::new);
    return MessageFormat.format(message, strings);
  }
}
