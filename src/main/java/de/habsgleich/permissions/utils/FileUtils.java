package de.habsgleich.permissions.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Class to manage file utilities
 *
 * <p>Created: 14.10.2022
 *
 * @author HabsGleich
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class FileUtils {

  /**
   * Copy internal files to an target {@link Path}
   *
   * @param classLoader from the file
   * @param file to copy
   * @param target to deploy
   */
  @SneakyThrows
  public static void doInternalCopy(ClassLoader classLoader, String file, Path target) {
    if (Files.notExists(target)) {
      Files.createDirectories(target);
    }
    try (InputStream inputStream = classLoader.getResourceAsStream(file)) {
      doCopy(inputStream, target);
    } catch (final IOException ex) {
      ex.printStackTrace();
    }
  }

  /**
   * Copy a resource as an {@link InputStream} to a target deploy {@link Path}
   *
   * @param inputStream file input stream
   * @param path to deploy
   * @param options for the copy
   */
  public static void doCopy(InputStream inputStream, Path path, CopyOption... options) {
    try {
      Files.copy(inputStream, path, options);
    } catch (final IOException ex) {
      ex.printStackTrace();
    }
  }
}
