package de.habsgleich.permissions.utils;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Created: 17.10.2022
 *
 * @author HabsGleich
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TimeFormatter {

  public static long of(String[] value) {
    long timeout = 0L;
    for (String input : value) {
      final StringBuilder digits = new StringBuilder();
      for (char character : input.toCharArray()) {
        if (!Character.isDigit(character)) {
          continue;
        }
        digits.append(character);
      }

      final TimeUnitAdapter adapter = TimeUnitAdapter.of(input.replace(digits.toString(), ""));
      if (adapter == null) {
        continue;
      }
      timeout += adapter.getTimeUnit().toMillis(Long.parseLong(digits.toString()));
    }
    return timeout;
  }

  @Getter
  public enum TimeUnitAdapter {
    DAYS(TimeUnit.DAYS, "d", "day", "days"),
    HOURS(TimeUnit.HOURS, "h", "hour", "hours"),
    MINUTES(TimeUnit.MINUTES, "m", "min", "minute", "minutes"),
    SECONDS(TimeUnit.SECONDS, "s", "sec", "second", "seconds");

    private final TimeUnit timeUnit;
    private final Set<String> ids;

    TimeUnitAdapter(TimeUnit timeUnit, String... ids) {
      this.timeUnit = timeUnit;
      this.ids = Set.of(ids);
    }

    public static TimeUnitAdapter of(String id) {
      for (TimeUnitAdapter adapter : values()) {
        if (!adapter.ids.contains(id)) {
          continue;
        }
        return adapter;
      }
      return null;
    }
  }
}
