package de.habsgleich.permissions.signs;

import de.habsgleich.permissions.serialization.SimpleLocation;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Config with locations for informative sings
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
@Data
public final class InformativeSignConfig {

  private final List<SimpleLocation> signEntries = new ArrayList<>();
}
