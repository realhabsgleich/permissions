package de.habsgleich.permissions.signs;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import de.habsgleich.permissions.serialization.SimpleLocation;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Bukkit;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;

/**
 * Driver for informative sings
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class InformativeSignDriver {
  private static final File CONFIG_FILE = new File("plugins/permissions", "informative_sings.json");

  static {
    if (!CONFIG_FILE.exists()) {
      try {
        CONFIG_FILE.getParentFile().mkdirs();
        CONFIG_FILE.createNewFile();

        Files.write(
            CONFIG_FILE.toPath(),
            Collections.singleton(PermissionPlugin.GSON.toJson(new InformativeSignConfig())),
            StandardCharsets.UTF_8);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private final PermissionPlugin plugin;
  private final InformativeSignConfig config;

  /**
   * Creates a new {@link InformativeSignDriver} instance
   *
   * @param plugin of the {@link PermissionPlugin}
   * @return a new instance
   */
  @SneakyThrows
  public static InformativeSignDriver create(PermissionPlugin plugin) {
    return new InformativeSignDriver(
        plugin,
        PermissionPlugin.GSON.fromJson(
            Files.readString(CONFIG_FILE.toPath(), StandardCharsets.UTF_8),
            InformativeSignConfig.class));
  }

  /** Update informative sings for all {@link Player} */
  public synchronized void updateInformativeSings() {
    for (PermissionUser user : plugin.getPermissionDriver().getCachedUsers()) {
      final boolean expire = user.getExpire().isPresent();
      final Player player = Bukkit.getPlayer(user.getUniqueId());
      if (player == null || !player.isOnline()) {
        continue;
      }
      for (SimpleLocation signEntry : config.getSignEntries()) {
        player.sendSignChange(
            signEntry.toBukkitLocation(),
            List.of(
                Component.text(player.getName()),
                Component.text(user.getPermissionGroup().getName(), NamedTextColor.YELLOW),
                Component.text("Expire:", NamedTextColor.BLACK),
                Component.text(
                    expire
                        ? "§6" + PermissionPlugin.EXPIRE_FORMAT.format(user.getExpire().orElse(0L))
                        : "§6§lNEVER")));
      }
    }
  }

  @SneakyThrows
  public void registerSign(Sign sign) {
    this.config.getSignEntries().add(SimpleLocation.ofBukkit(sign.getLocation()));
    Files.write(
        CONFIG_FILE.toPath(),
        Collections.singleton(PermissionPlugin.GSON.toJson(this.config)),
        StandardCharsets.UTF_8);
  }
}
