package de.habsgleich.permissions.serialization;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 * A serializable location class
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SimpleLocation {

  private final double x, y, z;
  private final String world;

  public Location toBukkitLocation() {
    return new Location(Bukkit.getWorld(this.world), this.x, this.y, this.z);
  }

  public static SimpleLocation ofBukkit(Location location) {
    return new SimpleLocation(location.getX(), location.getY(), location.getZ(), location.getWorld().getName());
  }
}
