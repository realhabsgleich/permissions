package de.habsgleich.permissions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.habsgleich.permissions.commands.PermissionCommand;
import de.habsgleich.permissions.commands.PermissionSignCommand;
import de.habsgleich.permissions.commands.RankCommand;
import de.habsgleich.permissions.language.LanguageManager;
import de.habsgleich.permissions.listener.PlayerJoinListener;
import de.habsgleich.permissions.permissible.bukkit.PermissionPresence;
import de.habsgleich.permissions.repository.PermissionDriver;
import de.habsgleich.permissions.repository.sql.SQLDriver;
import de.habsgleich.permissions.signs.InformativeSignDriver;
import de.habsgleich.permissions.tasks.InformativeSignUpdaterTask;
import de.habsgleich.permissions.tasks.RankExpireTask;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Objects;

/**
 * Bukkit bootstrap for the permission plugin
 *
 * <p>Created: 11.10.2022
 *
 * @author HabsGleich
 */
@Getter
public class PermissionPlugin extends JavaPlugin {
  public static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
  public static final DateFormat EXPIRE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

  private SQLDriver sqlDriver;
  private PermissionDriver permissionDriver;
  private InformativeSignDriver signDriver;
  private PermissionPresence permissionPresence;
  private FileConfiguration config;

  @SneakyThrows
  @Override
  public void onEnable() {
    saveDefaultConfig();

    this.config = super.getConfig();

    if (Objects.equals(config.getString("PostgreSQL.user"), "username")) {
      getLogger()
          .warning(
              "[Permissions] Default properties found! please enter your personal "
                  + "properties into the \"plugins/permissions/config.yml\"");
      Bukkit.getPluginManager().disablePlugin(this);
      return;
    }

    final Collection<String> languages = this.config.getStringList("Language.languages");
    LanguageManager.copyInternalFiles(languages);
    LanguageManager.load(this.config.getString("language.default"), languages);

    this.sqlDriver = SQLDriver.create(this);
    this.sqlDriver.connect();

    this.permissionDriver = PermissionDriver.create(this);
    this.permissionDriver
        .initGroups()
        .thenAccept(unused -> this.permissionDriver.loadGroupPermissions());

    this.signDriver = InformativeSignDriver.create(this);

    this.permissionPresence = new PermissionPresence(this);

    registerCommand(new PermissionCommand(this));
    registerCommand(new RankCommand(this));
    registerCommand(new PermissionSignCommand(this));

    Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(this), this);

    getServer()
        .getScheduler()
        .runTaskTimerAsynchronously(this, new RankExpireTask(this), 5 * 20L, 5 * 20L);
    getServer()
        .getScheduler()
        .runTaskTimerAsynchronously(this, new InformativeSignUpdaterTask(this), 2 * 20L, 2 * 20L);
  }

  private void registerCommand(Command command) {
    Bukkit.getCommandMap().register(command.getName(), command);
  }
}
