package de.habsgleich.permissions.commands;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.language.LanguageManager;
import de.habsgleich.permissions.permissible.group.PermissionGroup;
import de.habsgleich.permissions.utils.TimeFormatter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.function.Consumer;

/**
 * Command to manage permissions ingame
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
public final class PermissionCommand extends Command {
  private final PermissionPlugin plugin;

  public PermissionCommand(PermissionPlugin plugin) {
    super(
        "permissions",
        "Command to manage the permissions system",
        "/permissions",
        Collections.singletonList("perms"));
    this.plugin = plugin;
  }

  @Override
  public boolean execute(@NotNull CommandSender sender, @NotNull String s, @NotNull String[] args) {
    if (!sender.hasPermission("permissions.command.perms")) {
      sender.sendMessage(LanguageManager.get("no-permissions"));
      return false;
    }
    if (args.length == 1 && args[0].equalsIgnoreCase("groups")) {
      final Map<String, PermissionGroup> groupMap =
          plugin.getPermissionDriver().getPermissionGroupCache();
      if (groupMap.isEmpty()) {
        sender.sendMessage(LanguageManager.get("groups-empty"));
        return false;
      }
      sender.sendMessage(LanguageManager.get("group-list-header"));
      for (PermissionGroup group : groupMap.values()) {
        sender.sendMessage(
            LanguageManager.get(
                group.isDefaultGroup() ? "group-list-entry-standard" : "group-list-entry",
                group.getName(),
                group.getPrefix()));
      }
      return false;
    }

    if (args.length == 2 && args[0].equalsIgnoreCase("info")) {
      plugin
          .getPermissionDriver()
          .getGroup(args[1])
          .ifPresentOrElse(
              new Consumer<>() {
                @Override
                public void accept(PermissionGroup permissionGroup) {
                  if (permissionGroup.getPermissions().isEmpty()) {
                    sender.sendMessage(
                        LanguageManager.get(
                            "group-info-empty-permissions", permissionGroup.getName()));
                    return;
                  }
                  sender.sendMessage(
                      LanguageManager.get("group-info-header", permissionGroup.getName()));
                  for (String permission : permissionGroup.getPermissions()) {
                    sender.sendMessage(LanguageManager.get("group-info-entry", permission));
                  }
                }
              },
              () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[1])));
      return false;
    }

    if (args.length == 5
        && args[0].equalsIgnoreCase("group")
        && args[3].equalsIgnoreCase("permission")) {
      plugin
          .getPermissionDriver()
          .getGroup(args[1])
          .ifPresentOrElse(
              permissionGroup -> {
                final String permission = args[4];
                switch (args[2].toUpperCase()) {
                  case "ADD" -> {
                    plugin.getPermissionDriver().addPermission(permissionGroup, permission);
                    sender.sendMessage(
                        LanguageManager.get(
                            "group-permission-add", permission, permissionGroup.getName()));
                  }
                  case "REMOVE" -> {
                    plugin.getPermissionDriver().removePermission(permissionGroup, permission);
                    sender.sendMessage(
                        LanguageManager.get(
                            "group-permission-remove", permission, permissionGroup.getName()));
                  }
                  default -> sender.sendMessage(LanguageManager.get("command-perms-help"));
                }
              },
              () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[1])));
      return false;
    }

    if (args.length == 3
        && args[0].equalsIgnoreCase("create")
        && args[1].equalsIgnoreCase("group")) {
      final boolean exist = plugin.getPermissionDriver().getGroup(args[2]).isPresent();
      if (exist) {
        sender.sendMessage(LanguageManager.get("group-already-exist", args[2]));
        return false;
      }

      final PermissionGroup group = new PermissionGroup(args[2], "§7", 99, false);
      plugin.getPermissionDriver().createGroup(group);
      sender.sendMessage(LanguageManager.get("group-created", group.getName()));
      return false;
    }

    if (args.length == 4
        && args[0].equalsIgnoreCase("group")
        && args[2].equalsIgnoreCase("default")) {
      plugin
          .getPermissionDriver()
          .getGroup(args[1])
          .ifPresentOrElse(
              new Consumer<>() {
                @Override
                public void accept(PermissionGroup permissionGroup) {
                  plugin.getPermissionDriver().updateDefaultGroup(permissionGroup);
                  sender.sendMessage(
                      LanguageManager.get("group-is-now-default", permissionGroup.getName()));
                }
              },
              () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[1])));
      return false;
    }

    if (args.length >= 4
        && args[0].equalsIgnoreCase("group")
        && args[2].equalsIgnoreCase("prefix")) {
      plugin
          .getPermissionDriver()
          .getGroup(args[1])
          .ifPresentOrElse(
              new Consumer<>() {
                @Override
                public void accept(PermissionGroup permissionGroup) {
                  final StringBuilder prefixBuilder = new StringBuilder();
                  for (int i = 3; i < args.length; i++) {
                    prefixBuilder.append(args[i].replace("&", "§")).append(" ");
                  }
                  prefixBuilder.deleteCharAt(
                      prefixBuilder.length() - 1); // Delete empty char at the end

                  plugin
                      .getPermissionDriver()
                      .updatePrefix(permissionGroup, prefixBuilder.toString());
                  sender.sendMessage(
                      LanguageManager.get(
                          "group-prefix-update",
                          permissionGroup.getName(),
                          prefixBuilder.toString()));
                }
              },
              () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[1])));
      return false;
    }

    if (args.length >= 4
        && args[0].equalsIgnoreCase("group")
        && args[2].equalsIgnoreCase("sortid")) {
      plugin
          .getPermissionDriver()
          .getGroup(args[1])
          .ifPresentOrElse(
              new Consumer<>() {
                @Override
                public void accept(PermissionGroup permissionGroup) {
                  try {
                    int sortId = Integer.parseInt(args[3]);
                    plugin.getPermissionDriver().updateSortId(permissionGroup, sortId);
                    sender.sendMessage(
                        LanguageManager.get(
                            "group-sortid-update", permissionGroup.getName(), sortId));
                  } catch (NumberFormatException e) {
                    sender.sendMessage(LanguageManager.get("provide-valid-value", args[1]));
                  }
                }
              },
              () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[1])));
      return false;
    }

    if (args.length >= 4
        && args[0].equalsIgnoreCase("user")
        && args[2].equalsIgnoreCase("setgroup")) {
      try {
        final Optional<Long> expire =
            args.length >= 5
                ? Optional.of(
                    System.currentTimeMillis()
                        + TimeFormatter.of(Arrays.copyOfRange(args, 4, args.length)))
                : Optional.empty();
        plugin
            .getPermissionDriver()
            .getGroup(args[3])
            .ifPresentOrElse(
                permissionGroup -> {
                  final Player target = Bukkit.getPlayer(args[1]);
                  if (target == null) {
                    sender.sendMessage(LanguageManager.get("player-not-online"));
                    return;
                  }
                  plugin
                      .getPermissionDriver()
                      .updateUserGroup(target.getUniqueId(), permissionGroup, expire);
                  sender.sendMessage(
                      LanguageManager.get(
                          "user-group-set", target.getName(), permissionGroup.getName()));

                  plugin.getPermissionPresence().updateNameTags(target);
                },
                () -> sender.sendMessage(LanguageManager.get("group-does-not-exist", args[3])));
      } catch (NumberFormatException e) {
        sender.sendMessage("Format error");
      }
      return false;
    }
    sender.sendMessage(LanguageManager.get("command-perms-help"));
    return false;
  }

  @Override
  public @NotNull List<String> tabComplete(
      @NotNull CommandSender sender, @NotNull String alias, @NotNull String[] args)
      throws IllegalArgumentException {
    if (args.length == 2 && args[0].equalsIgnoreCase("group")) {
      return List.copyOf(plugin.getPermissionDriver().getPermissionGroupCache().keySet());
    }
    if (args.length == 2 && args[0].equalsIgnoreCase("user")) {
      return Bukkit.getOnlinePlayers().stream().map(Player::getName).toList();
    }
    return Collections.emptyList();
  }
}
