package de.habsgleich.permissions.commands;

import com.destroystokyo.paper.MaterialTags;
import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.language.LanguageManager;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created: 17.10.2022
 *
 * @author HabsGleich
 */
public final class PermissionSignCommand extends Command {
  private final PermissionPlugin plugin;

  public PermissionSignCommand(PermissionPlugin plugin) {
    super(
        "permission-sign",
        "Register a new informative sign",
        "/permission-sign",
        List.of("informative-sign", "perm-sign", "info-sign"));
    this.plugin = plugin;
  }

  @Override
  public boolean execute(
      @NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] strings) {
    if (!(commandSender instanceof Player player)) {
      commandSender.sendMessage(LanguageManager.get("must-be-a-player"));
      return false;
    }
    if (!player.hasPermission("permissions.command.sings")) {
      player.sendMessage(LanguageManager.get("no-permissions"));
      return false;
    }

    final Block block = player.getTargetBlock(5);
    if (block == null || !MaterialTags.SIGNS.isTagged(block)) {
      player.sendMessage(LanguageManager.get("command-sings-no-sign"));
      return false;
    }
    plugin.getSignDriver().registerSign((Sign) block.getState());
    player.sendMessage(LanguageManager.get("command-sings-registered"));
    return false;
  }
}
