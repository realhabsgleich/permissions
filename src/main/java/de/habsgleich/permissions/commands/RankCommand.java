package de.habsgleich.permissions.commands;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.language.LanguageManager;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Command to show the own rank
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
public final class RankCommand extends Command {
  private final PermissionPlugin plugin;

  public RankCommand(PermissionPlugin plugin) {
    super("rank", "Show details of your current rank", "/rank", Collections.singletonList("rang"));
    this.plugin = plugin;
  }

  @Override
  public boolean execute(
      @NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] args) {
    if (!(commandSender instanceof Player player)) {
      commandSender.sendMessage(
          "Hmm.. seems like you're the console... I think you are the boss of all time");
      return false;
    }
    plugin
        .getPermissionDriver()
        .getUser(player.getUniqueId())
        .thenAccept(
            new Consumer<>() {
              @Override
              public void accept(Optional<PermissionUser> permissionUser) {
                permissionUser
                    .orElseThrow()
                    .getExpire()
                    .ifPresentOrElse(
                        new Consumer<>() {
                          @Override
                          public void accept(Long expire) {
                            player.sendMessage(
                                LanguageManager.get(
                                    "command-rank-info",
                                    permissionUser.get().getPermissionGroup().getName(),
                                    PermissionPlugin.EXPIRE_FORMAT.format(expire)));
                          }
                        },
                        new Runnable() {
                          @Override
                          public void run() {
                            player.sendMessage(
                                LanguageManager.get(
                                    "command-rank-info-lifetime",
                                    permissionUser.get().getPermissionGroup().getName()));
                          }
                        });
              }
            })
        .exceptionally(
            throwable -> {
              player.sendMessage(LanguageManager.get("error-occurred", throwable.getMessage()));
              throwable.printStackTrace();
              return null;
            });
    return false;
  }
}
