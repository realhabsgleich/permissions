package de.habsgleich.permissions.repository.sql;

import de.habsgleich.permissions.PermissionPlugin;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.bukkit.configuration.file.FileConfiguration;
import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created: 17.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class SQLDriver {
  private static final String CONNECTION_STRING = "jdbc:postgresql://%s/%s";

  private final PermissionPlugin plugin;
  @Getter
  private Connection connection;

  public static SQLDriver create(PermissionPlugin plugin) {
    return new SQLDriver(plugin);
  }

  /** Initialize the connection to the database and initializing tables */
  @SneakyThrows
  public void connect() {
    DriverManager.registerDriver(new Driver());

    final FileConfiguration config = plugin.getConfig();
    this.connection =
        DriverManager.getConnection(
            CONNECTION_STRING.formatted(
                config.getString("PostgreSQL.hostname"), config.getString("PostgreSQL.database")),
            config.getString("PostgreSQL.user"),
            config.getString("PostgreSQL.password"));
    plugin.getLogger().info("[Permissions] The connection to the database was established");

    this.initTables();
  }

  /** Initialize the required tables and create them if they're not exist */
  public void initTables() {
    plugin.getLogger().info("[Permissions] Initializing tables...");
    SQLBuilder.of(this.connection)
        .withQuery(
            "CREATE TABLE IF NOT EXISTS permission_users ("
                + "uniqueId CHAR(36) PRIMARY KEY NOT NULL, "
                + "\"group\" VARCHAR NOT NULL, "
                + "expire BIGINT"
                + ");")
        .execute();
    SQLBuilder.of(this.connection)
        .withQuery(
            "CREATE TABLE IF NOT EXISTS permission_groups ("
                + "name VARCHAR PRIMARY KEY NOT NULL, "
                + "prefix VARCHAR NOT NULL, "
                + "sortId INT NOT NULL, "
                + "\"default\" BOOLEAN NOT NULL"
                + ");")
        .execute();
    SQLBuilder.of(this.connection)
        .withQuery(
            "CREATE TABLE IF NOT EXISTS permissions ("
                + "\"group\" VARCHAR NOT NULL, "
                + "permission VARCHAR NOT NULL "
                + ");")
        .execute();
    plugin.getLogger().info("[Permissions] Tables has been initialized!");
  }
}
