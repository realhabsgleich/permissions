package de.habsgleich.permissions.repository.sql;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * Builder to execute PostgreSQL statements
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class SQLBuilder {
  private final Connection connection;
  private String query;
  private Object[] args;

  /**
   * Creates an {@link SQLBuilder} instance for the provided connection
   *
   * @param connection to execute
   * @return an {@link SQLBuilder} instance
   */
  public static SQLBuilder of(Connection connection) {
    return new SQLBuilder(connection);
  }

  /**
   * Set the query which will be executed
   *
   * @param query to be executed
   */
  public SQLBuilder withQuery(String query) {
    this.query = query;
    return this;
  }

  /**
   * Set the arguments which will be inserted to the query string
   *
   * @param args to insert
   */
  public SQLBuilder withArgs(Object... args) {
    this.args = args;
    return this;
  }

  /**
   * Executes the query asynchronously
   *
   * @return an {@link CompletableFuture} with a boolean if the query has been executed successfully
   */
  public CompletableFuture<Boolean> execute() {
    return CompletableFuture.supplyAsync(
        () -> {
          try {
            final PreparedStatement statement = connection.prepareStatement(this.query);

            // Insert statement arguments
            if (args != null) {
              int index = 1;
              for (Object arg : args) {
                statement.setObject(index++, arg);
              }
            }
            return statement.execute();
          } catch (SQLException e) {
            System.err.println("[Permissions] Cloud not execute sql statement! ");
            e.printStackTrace();
            return false;
          }
        });
  }

  /**
   * Executes the query asynchronously
   *
   * @return an {@link CompletableFuture} with an {@link Optional} which include the {@link ResultSet}
   */
  public CompletableFuture<ResultSet> executeQuery() {
    return CompletableFuture.supplyAsync(
        () -> {
          try {
            final PreparedStatement statement = connection.prepareStatement(this.query);

            // Insert statement arguments
            if (args != null) {
              int index = 1;
              for (Object arg : args) {
                statement.setObject(index++, arg);
              }
            }
            return statement.executeQuery();
          } catch (SQLException e) {
            System.err.println("[Permissions] Cloud not execute sql query statement! ");
            e.printStackTrace();
            return null;
          }
        });
  }
}
