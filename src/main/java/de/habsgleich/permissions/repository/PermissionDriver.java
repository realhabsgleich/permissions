package de.habsgleich.permissions.repository;

import com.google.common.base.Preconditions;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableList;
import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.permissible.group.PermissionGroup;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import de.habsgleich.permissions.repository.sql.SQLBuilder;
import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.UnmodifiableView;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * A repository to manage permissions in PostgreSQL
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
@Data
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class PermissionDriver {
  private static final Cache<UUID, PermissionUser> PERMISSION_USER_CACHE =
      CacheBuilder.newBuilder().expireAfterWrite(15L, TimeUnit.MINUTES).build();

  private final PermissionPlugin plugin;
  private final Connection connection;
  private final Map<String, PermissionGroup> permissionGroupCache = new ConcurrentHashMap<>();
  private PermissionGroup defaultGroup;

  /**
   * Creates a new {@link PermissionDriver}
   *
   * @param plugin the instance of the {@link PermissionPlugin}
   * @return a new {@link PermissionDriver} instance
   */
  public static PermissionDriver create(PermissionPlugin plugin) {
    return new PermissionDriver(plugin, plugin.getSqlDriver().getConnection());
  }

  /**
   * Initialize groups from the database in the local cache
   *
   * @return a {@link CompletableFuture} which is completed when the task is finished
   */
  public CompletableFuture<Void> initGroups() {
    final CompletableFuture<Void> future = new CompletableFuture<>();
    this.getGroups()
        .thenAccept(
            permissionGroups -> {
              for (PermissionGroup group : permissionGroups) {
                this.permissionGroupCache.put(group.getName(), group);
                if (group.isDefaultGroup()) {
                  this.defaultGroup = group;
                }
              }
              if (this.defaultGroup == null) {
                plugin
                    .getLogger()
                    .info(
                        "[Permissions] No default group found! creating group 'User' as default group");
                final PermissionGroup def = new PermissionGroup("User", "§7", 99, true);
                this.defaultGroup = def;
                createGroup(def);
              }
              plugin.getLogger().info("[Permissions] Group cache has been initialized");
              future.complete(null);
            });
    return future;
  }

  /** Query all permissions from the database and insert it in the groups */
  public void loadGroupPermissions() {
    SQLBuilder.of(this.connection)
        .withQuery("SELECT * FROM permissions;")
        .executeQuery()
        .thenAccept(
            new Consumer<>() {
              @SneakyThrows
              @Override
              public void accept(ResultSet resultSet) {
                while (resultSet.next()) {
                  getGroup(resultSet.getString("group"))
                      .ifPresent(
                          new Consumer<>() {
                            @SneakyThrows
                            @Override
                            public void accept(PermissionGroup permissionGroup) {
                              permissionGroup
                                  .getPermissions()
                                  .add(resultSet.getString("permission"));
                            }
                          });
                }
                plugin.getLogger().info("[Permissions] Successfully loaded group permissions");
              }
            })
        .exceptionally(
            throwable -> {
              throwable.printStackTrace();
              return null;
            });
  }

  /**
   * Query all permission groups from postgre
   *
   * @return a {@link Collection} with all existing {@link PermissionGroup}'s
   */
  public CompletableFuture<Collection<PermissionGroup>> getGroups() {
    final CompletableFuture<Collection<PermissionGroup>> future = new CompletableFuture<>();
    SQLBuilder.of(this.connection)
        .withQuery("SELECT * FROM permission_groups;")
        .executeQuery()
        .thenAccept(
            new Consumer<>() {
              @SneakyThrows
              @Override
              public void accept(ResultSet resultSet) {
                final Collection<PermissionGroup> groups = new ArrayList<>();
                while (resultSet.next()) {
                  groups.add(
                      new PermissionGroup(
                          resultSet.getString("name"),
                          resultSet.getString("prefix"),
                          resultSet.getInt("sortId"),
                          resultSet.getBoolean("default")));
                }
                future.complete(groups);
              }
            })
        .exceptionally(
            throwable -> {
              future.completeExceptionally(throwable);
              return null;
            });
    return future;
  }

  /**
   * Creates a new {@link PermissionGroup}
   *
   * @param group instance of the new group
   */
  public void createGroup(@NotNull PermissionGroup group) {
    if (group.isDefaultGroup()) {
      this.defaultGroup = group;
    }
    this.permissionGroupCache.put(group.getName(), group);
    SQLBuilder.of(this.connection)
        .withQuery(
            "INSERT INTO permission_groups "
                + "(name, prefix, sortId, \"default\") "
                + "VALUES "
                + "(?, ?, ?, ?);")
        .withArgs(group.getName(), group.getPrefix(), group.getSortId(), group.isDefaultGroup())
        .execute();
  }

  /**
   * Get a {@link PermissionGroup} from the local cache
   *
   * @param name of the group
   * @return the cached group
   */
  public Optional<PermissionGroup> getGroup(String name) {
    return Optional.ofNullable(this.permissionGroupCache.get(name));
  }

  /**
   * Set a new default group
   *
   * @param permissionGroup which will be the new default group
   */
  public void updateDefaultGroup(PermissionGroup permissionGroup) {
    Preconditions.checkNotNull(permissionGroup);
    if (this.defaultGroup != null) {
      this.defaultGroup.setDefaultGroup(false);
      SQLBuilder.of(this.connection)
          .withQuery("UPDATE permission_groups SET \"default\" = ? WHERE name = ?;")
          .withArgs(false, defaultGroup.getName())
          .execute();
    }
    permissionGroup.setDefaultGroup(true);
    SQLBuilder.of(this.connection)
        .withQuery("UPDATE permission_groups SET \"default\" = ? WHERE name = ?;")
        .withArgs(true, permissionGroup.getName())
        .execute();
    this.defaultGroup = permissionGroup;
  }

  /**
   * Assign a new group to a player
   *
   * @param uniqueId of the player
   * @param permissionGroup new group
   * @param expire milliseconds when the group will expire, provide {@link Optional#empty()} if the
   *     group is lifetime
   */
  @SneakyThrows
  public void updateUserGroup(
      UUID uniqueId, PermissionGroup permissionGroup, Optional<Long> expire) {
    Preconditions.checkNotNull(permissionGroup);
    if (PERMISSION_USER_CACHE.asMap().containsKey(uniqueId)) {
      final PermissionUser user = PERMISSION_USER_CACHE.asMap().get(uniqueId);
      user.setPermissionGroup(permissionGroup);
      user.setExpire(expire);
    }
    SQLBuilder.of(this.connection)
        .withQuery("UPDATE permission_users SET \"group\" = ?, expire = ? WHERE uniqueId = ?;")
        .withArgs(
            permissionGroup.getName(), expire.isEmpty() ? null : expire.get(), uniqueId.toString())
        .execute()
        .exceptionally(
            throwable -> {
              throwable.printStackTrace();
              return false;
            });
  }

  /**
   * Get a {@link PermissionUser} from the local cache, otherwise query from the database
   *
   * @param uniqueId of the player
   * @return a {@link Optional} with the {@link PermissionUser} instance
   */
  public CompletableFuture<Optional<PermissionUser>> getUser(UUID uniqueId) {
    if (PERMISSION_USER_CACHE.asMap().containsKey(uniqueId)) {
      return CompletableFuture.completedFuture(
          Optional.of(PERMISSION_USER_CACHE.asMap().get(uniqueId)));
    }
    final CompletableFuture<Optional<PermissionUser>> future = new CompletableFuture<>();
    SQLBuilder.of(this.connection)
        .withQuery("SELECT * FROM permission_users WHERE uniqueId = ?;")
        .withArgs(uniqueId.toString())
        .executeQuery()
        .thenAccept(
            new Consumer<>() {
              @SneakyThrows
              @Override
              public void accept(ResultSet resultSet) {
                if (!resultSet.next()) {
                  future.complete(Optional.empty());
                  return;
                }
                final String group = resultSet.getString("group");
                final long expire = resultSet.getLong("expire");
                final PermissionUser permissionUser =
                    new PermissionUser(
                        uniqueId,
                        getGroup(group).orElse(defaultGroup),
                        expire == 0 ? Optional.empty() : Optional.of(expire));
                PERMISSION_USER_CACHE.put(uniqueId, permissionUser);
                future.complete(Optional.of(permissionUser));
              }
            })
        .exceptionally(
            throwable -> {
              future.completeExceptionally(throwable);
              return null;
            });
    return future;
  }

  /**
   * Create a new {@link PermissionUser} instance
   *
   * @param uniqueId of the new player
   * @return a new {@link PermissionUser}
   */
  public PermissionUser createPlayer(UUID uniqueId) {
    final PermissionUser permissionUser =
        new PermissionUser(uniqueId, defaultGroup, Optional.empty());
    PERMISSION_USER_CACHE.put(uniqueId, permissionUser);
    SQLBuilder.of(this.connection)
        .withQuery(
            "INSERT INTO permission_users "
                + "(uniqueId, \"group\", expire) "
                + "VALUES "
                + "(?, ?, ?);")
        .withArgs(uniqueId.toString(), defaultGroup.getName(), null)
        .execute();
    return permissionUser;
  }

  /**
   * Add a permission to a {@link PermissionGroup}
   *
   * @param permissionGroup to add
   * @param permission which will be added
   */
  public void addPermission(PermissionGroup permissionGroup, String permission) {
    Preconditions.checkNotNull(permissionGroup);
    Preconditions.checkNotNull(permission);

    permissionGroup.getPermissions().add(permission);
    SQLBuilder.of(this.connection)
        .withQuery("INSERT INTO permissions " + "(\"group\", permission) " + "VALUES " + "(?, ?);")
        .withArgs(permissionGroup.getName(), permission)
        .execute()
        .exceptionally(
            throwable -> {
              throwable.printStackTrace();
              return null;
            });
  }

  /**
   * Remove a permission from a {@link PermissionGroup}
   *
   * @param permissionGroup to remove
   * @param permission which will be removed
   */
  public void removePermission(PermissionGroup permissionGroup, String permission) {
    Preconditions.checkNotNull(permissionGroup);
    Preconditions.checkNotNull(permission);

    permissionGroup.getPermissions().remove(permission);
    SQLBuilder.of(this.connection)
        .withQuery("DELETE FROM permissions WHERE \"group\" = ? AND permission = ?;")
        .withArgs(permissionGroup.getName(), permission)
        .execute();
  }

  /**
   * Update the prefix from a {@link PermissionGroup}
   *
   * @param permissionGroup to update
   * @param prefix new prefix
   */
  public void updatePrefix(PermissionGroup permissionGroup, String prefix) {
    Preconditions.checkNotNull(permissionGroup);
    Preconditions.checkNotNull(prefix);

    permissionGroup.setPrefix(prefix);
    SQLBuilder.of(this.connection)
        .withQuery("UPDATE permission_groups SET prefix = ? WHERE name = ?;")
        .withArgs(prefix, permissionGroup.getName())
        .execute();
  }

  /**
   * Update the sorting ID from a {@link PermissionGroup}
   *
   * @param permissionGroup to update
   * @param sortId new sortId
   */
  public void updateSortId(PermissionGroup permissionGroup, int sortId) {
    Preconditions.checkNotNull(permissionGroup);

    permissionGroup.setSortId(sortId);
    SQLBuilder.of(this.connection)
        .withQuery("UPDATE permission_groups SET sortId = ? WHERE name = ?;")
        .withArgs(sortId, permissionGroup.getName())
        .execute();
  }

  /**
   * Get all cached {@link PermissionUser}
   *
   * @return a {@link ImmutableList} with all cached {@link PermissionUser}
   */
  public @UnmodifiableView Collection<PermissionUser> getCachedUsers() {
    return ImmutableList.copyOf(PERMISSION_USER_CACHE.asMap().values());
  }
}
