package de.habsgleich.permissions.tasks;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.function.Consumer;

/**
 * Created: 16.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor
public final class RankExpireTask implements Runnable {
  private final PermissionPlugin plugin;

  @Override
  public void run() {
    for (PermissionUser user : plugin.getPermissionDriver().getCachedUsers()) {
      user.getExpire()
          .ifPresent(
              new Consumer<>() {
                @Override
                public void accept(Long expire) {
                  if (expire > System.currentTimeMillis()) {
                    return;
                  }
                  plugin
                      .getPermissionDriver()
                      .updateUserGroup(
                          user.getUniqueId(),
                          plugin.getPermissionDriver().getDefaultGroup(),
                          Optional.empty());
                  final Player toUpdate = Bukkit.getPlayer(user.getUniqueId());
                  if (toUpdate != null) {
                    plugin.getPermissionPresence().updateNameTags(toUpdate);
                  }
                }
              });
    }
  }
}
