package de.habsgleich.permissions.tasks;

import de.habsgleich.permissions.PermissionPlugin;
import lombok.RequiredArgsConstructor;

/**
 * Created: 17.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor
public final class InformativeSignUpdaterTask implements Runnable {
  private final PermissionPlugin plugin;

  @Override
  public void run() {
    plugin.getSignDriver().updateInformativeSings();
  }
}
