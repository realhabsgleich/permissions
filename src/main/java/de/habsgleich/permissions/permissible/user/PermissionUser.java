package de.habsgleich.permissions.permissible.user;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.permissible.PermissionContainer;
import de.habsgleich.permissions.permissible.group.PermissionGroup;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;
import java.util.UUID;

/**
 * Object that contains all information about a permission user
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
@Getter
@Setter
@AllArgsConstructor
public final class PermissionUser implements PermissionContainer {
  private final UUID uniqueId;
  private PermissionGroup permissionGroup;
  private Optional<Long> expire;

  /** {@inheritDoc} */
  @Override
  public boolean hasPermission(String permission) {
    return this.permissionGroup.getPermissions().contains(permission);
  }

  public String formatExpire() {
    if (expire.isEmpty()) {
      return "LIFETIME";
    }
    return PermissionPlugin.EXPIRE_FORMAT.format(expire.get());
  }
}
