package de.habsgleich.permissions.permissible.group;

import de.habsgleich.permissions.permissible.PermissionContainer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Object that contains all information about a permission group
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
@Getter
@Setter
@AllArgsConstructor
public final class PermissionGroup implements PermissionContainer {
  private final String name;
  private String prefix;
  private int sortId;
  private boolean defaultGroup;

  private final Set<String> permissions = Collections.synchronizedSet(new HashSet<>());

  /** {@inheritDoc} */
  @Override
  public boolean hasPermission(String permission) {
    return permissions.contains(permission);
  }
}
