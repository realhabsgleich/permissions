package de.habsgleich.permissions.permissible.bukkit;

import com.google.common.base.Preconditions;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;

/**
 * Inject our own {@link SimplePermissible} into the bukkit {@link Player} with reflections
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
public final class PermissionInjector {
  private static final Field permissionsField;

  static {
    try {
      String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
      permissionsField =
          Class.forName("org.bukkit.craftbukkit." + version + ".entity.CraftHumanEntity")
              .getDeclaredField("perm");
      permissionsField.setAccessible(true);
    } catch (final ClassNotFoundException | NoSuchFieldException ex) {
      throw new RuntimeException("Error while obtaining bukkit perm fields", ex);
    }
  }

  /**
   * Inject the own {@link SimplePermissible} to check permissions with {@link
   * Player#hasPermission(String)}
   *
   * @param player to be injected
   * @param permissionUser from the target player
   */
  public static void injectPlayer(@NotNull Player player, PermissionUser permissionUser) {
    Preconditions.checkNotNull(permissionsField);
    try {
      permissionsField.set(player, new SimplePermissible(player, permissionUser));
    } catch (final IllegalAccessException ex) {
      ex.printStackTrace();
    }
  }
}
