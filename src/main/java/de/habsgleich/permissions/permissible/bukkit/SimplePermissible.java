package de.habsgleich.permissions.permissible.bukkit;

import de.habsgleich.permissions.permissible.user.PermissionUser;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;

/**
 * Bukkit permissible object to check our own permissions
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
public final class SimplePermissible extends PermissibleBase {
  private final PermissionUser user;

  public SimplePermissible(Player player, PermissionUser user) {
    super(player);
    this.user = user;
  }

  @Override
  public boolean hasPermission(@NotNull String inName) {
    return user.hasPermission(inName);
  }

  @Override
  public boolean hasPermission(@NotNull Permission perm) {
    return user.hasPermission(perm.getName());
  }

  @Override
  public boolean isPermissionSet(@NotNull String name) {
    return user.hasPermission(name);
  }

  @Override
  public boolean isPermissionSet(@NotNull Permission perm) {
    return user.hasPermission(perm.getName());
  }
}
