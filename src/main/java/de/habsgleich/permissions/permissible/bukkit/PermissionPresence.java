package de.habsgleich.permissions.permissible.bukkit;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.permissible.group.PermissionGroup;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Manage the presence of a player
 *
 * <p>Created: 15.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor
public final class PermissionPresence {
  private final PermissionPlugin plugin;

  /**
   * Update the name tags over a players head and in the tablist
   *
   * @param player to be updated
   */
  @SneakyThrows
  public void updateNameTags(Player player) {
    Bukkit.getScheduler()
        .getMainThreadExecutor(this.plugin)
        .execute(
            () -> {
              // Set new scoreboard if the player has the default bukkit scoreboard
              if (player
                  .getScoreboard()
                  .equals(Bukkit.getScoreboardManager().getMainScoreboard())) {
                player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
              }

              try {
                final PermissionUser permissionUser =
                    plugin
                        .getPermissionDriver()
                        .getUser(player.getUniqueId())
                        .get(350L, TimeUnit.MILLISECONDS)
                        .orElseThrow();
                Bukkit.getOnlinePlayers()
                    .forEach(
                        onlinePlayer -> {
                          if (onlinePlayer
                              .getScoreboard()
                              .equals(Bukkit.getScoreboardManager().getMainScoreboard())) {
                            onlinePlayer.setScoreboard(
                                Bukkit.getScoreboardManager().getNewScoreboard());
                          }
                          registerTeam(player, onlinePlayer, permissionUser);
                          try {
                            final PermissionUser onlinePermissionUser =
                                plugin
                                    .getPermissionDriver()
                                    .getUser(onlinePlayer.getUniqueId())
                                    .get(350L, TimeUnit.MILLISECONDS)
                                    .orElseThrow();
                            registerTeam(onlinePlayer, player, onlinePermissionUser);
                          } catch (InterruptedException | ExecutionException | TimeoutException e) {
                            throw new RuntimeException(e);
                          }
                        });
              } catch (InterruptedException | ExecutionException | TimeoutException e) {
                throw new RuntimeException(e);
              }
            });
  }

  /**
   * Register a new scoreboard {@link Team} for the player
   *
   * @param player to be registered
   * @param target to register the team
   * @param permissionUser of the player
   */
  private void registerTeam(Player player, Player target, PermissionUser permissionUser) {
    final PermissionGroup permissionGroup = permissionUser.getPermissionGroup();

    final String teamName = permissionGroup.getSortId() + player.getName();

    Team team = target.getScoreboard().getTeam(teamName);
    if (team == null) {
      team = target.getScoreboard().registerNewTeam(teamName);
    }

    team.prefix(Component.text(permissionGroup.getPrefix()));

    team.addEntry(player.getName());
    player.displayName(Component.text(permissionGroup.getPrefix() + player.getName()));
  }
}
