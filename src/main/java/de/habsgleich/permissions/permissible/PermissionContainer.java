package de.habsgleich.permissions.permissible;

/**
 * An interface for a permissible object
 *
 * <p>Created: 13.10.2022
 *
 * @author HabsGleich
 */
public interface PermissionContainer {

  /**
   * Checks if the object has the given permission
   *
   * @param permission value to check
   * @return a boolean whether the object has the permission
   */
  boolean hasPermission(String permission);
}
