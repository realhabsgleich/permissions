package de.habsgleich.permissions.listener;

import de.habsgleich.permissions.PermissionPlugin;
import de.habsgleich.permissions.language.LanguageManager;
import de.habsgleich.permissions.permissible.bukkit.PermissionInjector;
import de.habsgleich.permissions.permissible.user.PermissionUser;
import lombok.RequiredArgsConstructor;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Listen for a player join
 *
 * <p>Created: 14.10.2022
 *
 * @author HabsGleich
 */
@RequiredArgsConstructor
public final class PlayerJoinListener implements Listener {
  private final PermissionPlugin plugin;

  @EventHandler
  public void onJoin(PlayerJoinEvent event) {
    final Player player = event.getPlayer();
    event.joinMessage(Component.empty());

    plugin
        .getPermissionDriver()
        .getUser(player.getUniqueId())
        .thenAccept(
            new Consumer<>() {
              @Override
              public void accept(Optional<PermissionUser> permissionUser) {
                final PermissionUser user =
                    permissionUser.orElseGet(
                        new Supplier<>() {
                          @Override
                          public PermissionUser get() {
                            return plugin.getPermissionDriver().createPlayer(player.getUniqueId());
                          }
                        });
                PermissionInjector.injectPlayer(player, user);
                plugin.getPermissionPresence().updateNameTags(player);
                for (Player players : Bukkit.getOnlinePlayers()) {
                  players.sendMessage(
                      LanguageManager.get(
                          "join-message",
                          user.getPermissionGroup().getPrefix() + event.getPlayer().getName()));
                }
              }
            })
        .exceptionally(
            throwable -> {
              throwable.printStackTrace();
              return null;
            });
  }
}
