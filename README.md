
# Permissions

A very simple permission system for Spigot, mainly for educational purposes for Google Codestyle and PostgreSQL

As build tool I use maven

## Features

- Permission groups with timeout
- Group prefixes and sorting id's


## Language System

You can add Languages with files and ".properties" ending

**Default languages**
- de_DE
- en_US


## System-Permissions

- `permissions.command.perms` Manage permissions with '/permsissions'
- `permissions.command.sings` Manage informative sings ingame with '/permission-sign'
## Contributing

Contributions are always welcome!

Please make sure you use the google codestyle.

